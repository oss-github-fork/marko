---
"@marko/translator-tags": patch
"@marko/compiler": patch
"marko": patch
"@marko/translator-default": patch
"@marko/translator-interop-class-tags": patch
---

Support tracking the "input" using babels scope analysis.
